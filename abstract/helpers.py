from .cursing import *

#*************************************************************************


##########################################################################

class Atomic(object):
    def __init__(self, *args, **kwargs):
        self.trigger('initialize', *args, **kwargs)

    def trigger(self, method, *args, **kwargs):
        hnd = getattr(self, method, None)

        if callable(hnd):
            return hnd(*args, **kwargs)

        return None

    def log(self, severity, level, message, *args, **kwargs):
        ommited = ['DEBUG']

        if severity.upper() not in ommited:
            resp = message

            if len(args):
                resp = message % args

            if len(kwargs):
                resp = message % kwargs

            resp = (level * '\t') + resp

            print "[%s] %s" % (severity,resp)

#*************************************************************************

class Molecular(Atomic):
    def __init__(self, parent, narrow, *args, **kwargs):
        self._prn = parent
        self._nrw = narrow

        self.trigger('initialize', *args, **kwargs)

    parent = property(lambda self: self._prn)
    narrow = property(lambda self: self._nrw)

#*************************************************************************

class Factory:
    @classmethod
    def handlers(cls,narrow):
        if not hasattr(cls,'_reg'):
            setattr(cls,'_reg',{})

        if narrow not in cls._reg:
            cls._reg[narrow] = {}

        return cls._reg[narrow].iteritems()

    @classmethod
    def instance(cls,narrow,alias,*args,**kwargs):
        if not hasattr(cls,'_reg'):
            setattr(cls,'_reg',{})

        if narrow not in cls._reg:
            cls._reg[narrow] = {}

        resp = None

        if alias in cls._reg[narrow]:
            handler = cls._reg[narrow][alias]

            if callable(handler):
                resp = handler(*args,**kwargs)

        return resp

    @classmethod
    def register(cls,*args,**kwargs):
        def do_apply(handler,narrow,alias,**opts):
            if not hasattr(cls,'_reg'):
                setattr(cls,'_reg',{})

            if narrow not in cls._reg:
                cls._reg[narrow] = {}

            if alias not in cls._reg[narrow]:
                cls._reg[narrow][alias] = handler

            return handler

        return lambda hnd: do_apply(hnd,*args,**kwargs)

##########################################################################

@singleton
class Reactor(Atomic):
    def initialize(self):
        self._ext = {}

    def extend(self, *args, **kwargs):
        def do_apply(handler, alias, **opts):
            if alias not in self._ext:
                self._ext[alias] = handler(self,alias,**opts)

                setattr(self, alias, self._ext[alias])

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    #**************************************************************************

    class Extension(Molecular):
        def initialize(self):
            self.trigger('prepare')

