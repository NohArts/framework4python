from .helpers import *

##########################################################################

class Instance(Molecular):
    def initialize(self, *args, **kwargs):
        self._lst = {}

        self.trigger('prepare', *args, **kwargs)

    listing = property(lambda self: self._lst)

    def __getitem__(self, alias):
        return self._lst.get(alias,None)

    def depends(self, alias, version=None):
        if version in (None,False,0,'0'):
            version = '*'

        if alias in self._lst:
            if version!='*':
                self._lst[alias] = version
        else:
            self._lst[alias] = version

        return self

