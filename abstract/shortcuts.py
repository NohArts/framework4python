from .library import *

from .helpers import *

from .reactor import *

##########################################################################

import jinja2

tpl_env = jinja2.Environment(
    extensions=[
        #'jinja2.ext.autoescape',
        #'jinja2.ext.babel_extract',
        #'jinja2.ext.concat',
        #'jinja2.ext.contextfunction',
        #'jinja2.ext.do',
        #'jinja2.ext.extract_from_ast',
        #'jinja2.ext.i18n',
        #'jinja2.ext.import_string',
        #'jinja2.ext.iteritems',
        #'jinja2.ext.loopcontrols',
        #'jinja2.ext.nodes',
        #'jinja2.ext.string_types',
        #'jinja2.ext.with_',
        #'jinja2.ext.with_metaclass',
    ],
    loader=jinja2.loaders.FileSystemLoader(spath('template.d')),
)

#*************************************************************************

def render_text(source, context):
    tpl = tpl_env.compile(source)

    cnt = tpl.render(**context)

    return cnt

#*************************************************************************

def render_file(tpl_name, context):
    tpl = tpl_env.get_template(tpl_name)

    cnt = tpl.render(**context)

    return cnt

#*************************************************************************

def render_page(tpl_name, dest_path, context):
    cnt = render_file(tpl_name, context)

    with open(dest_path,'w+') as f:
        f.write(cnt)

    return cnt

