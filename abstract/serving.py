from .helpers import *

##########################################################################

class BaseWorker(Molecular):
    def initialize(self, *args, **kwargs):
        self._lst = {}

        self.trigger('prepare', *args, **kwargs)

    listing = property(lambda self: self._lst)

    def __getitem__(self, alias):
        return self._lst.get(alias,None)

    def depends(self, alias, version=None):
        if version in (None,False,0,'0'):
            version = '*'

        if alias in self._lst:
            if version!='*':
                self._lst[alias] = version
        else:
            self._lst[alias] = version

        return self

##########################################################################

@Factory.register('work','crossbar')
class CrossWorker(BaseWorker):
    def prepare(self, *args, **kwargs):
        self._work = {
        }

    def exports(self, instance, **ctx):
        resp = {
            "type" : "guest",
            "executable" : instance['prog'],
            "arguments" : [str(x) for x in instance['args']],
            "options" : {
                "env" : {
                    "vars" : dict([
                        (k,str(v))
                        for k,v in instance.get('vars', {}).iteritems()
                    ]),
                    "inherit" : True,
                },
                "workdir" : instance.get('work', '.')
            },
        }

        instance['dirs'] = instance.get('dirs', [])

        if len(instance['dirs']):
            resp['options']['watch'] = {
                "directories" : instance['dirs'],
                "action" : "restart"
            }

        return resp

#*************************************************************************

@Factory.register('work','supervisor')
class SuperWorker(BaseWorker):
    def prepare(self, *args, **kwargs):
        self._work = {
        }

    def exports(self):
        return resp

#*************************************************************************

@Factory.register('work','docker')
class DockerWorker(BaseWorker):
    def prepare(self, *args, **kwargs):
        self._work = {
        }

    def exports(self):
        return resp

#*************************************************************************

@Factory.register('work','forever')
class EverWorker(BaseWorker):
    def prepare(self, *args, **kwargs):
        self._work = {
        }

    def exports(self):
        return resp

#*************************************************************************

@Factory.register('work','unleash')
class LeashWorker(BaseWorker):
    def prepare(self, *args, **kwargs):
        self._work = {
        }

    def exports(self):
        return resp

