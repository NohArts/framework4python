import os, sys, click
import re, yaml, json
import random

##########################################################################

from datetime import datetime as dt
from urlparse import urlparse

import json,yaml
from collections import OrderedDict

#******************************************************************************

import types

def isclass(target): return isinstance(target, (types.TypeType, types.ClassType))

#******************************************************************************

import nltk, pattern

#******************************************************************************

from autobahn.twisted.wamp   import ApplicationSession as WampAppSession
from twisted.internet.defer  import inlineCallbacks    as WampCallbacks

#******************************************************************************

import numpy

#import bigtempo.core
#from flask_bigtempo import DatastoreAPI, BigtempoAPI

import pandas, tablib, records

#******************************************************************************

import rdflib # owlready

