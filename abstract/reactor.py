from .helpers import *

from .brewing import *
from .linking import *
from .packing import *
from .serving import *
from .working import *

##########################################################################

class NeuroChip(Atomic):
    def initialize(self, *args, **kwargs):
        self.title = "NeuroChip::PipeFlow"
        self._port = PortMapper()
        self._land = rpath('workdir','web','landing')

        self._path = {
            "cdn": {
                "type": "path",
                "paths": {}
            },
            "poll" : {
                "type" : "longpoll"
            }
        }

        self._lang = {}
        self._curr = None
        self._vern = []

        for key,hnd in Molecular.handlers('pkg'):
            self._lang[key] = hnd(self, key)

        self._data = []
        self._xlet = []
        self._apps = [
#    dict(type='parse',name='first',uuid='myAppId',keys={
#        'core': 'xxx', 'http': 'yyy', 'node': 'zzz'
#    }, flag=True, icon="MyAppIcon.png"),
#    dict(type='parse',name='second',uuid='myAppId',keys={
#        'core': 'xxx', 'http': 'yyy', 'node': 'zzz'
#    }, flag=True),
#    dict(type='parse',name='third',uuid='myAppId',keys={
#        'core': 'xxx', 'http': 'yyy', 'node': 'zzz'
#    }, flag=True)
        ]
        self._file = {}
        self._tool = []
        self._work = []
        self._serv = []

        self._bots = dict(
            hubot    = [
                "redis-brain.coffee",
                "giphy.coffee",
                "stallman.coffee",
                "reload.coffee",
                "hello",
            ],
            external = [
                'hubot-analytics',
                'hubot-markov',
            ],
            vtr = [
                "epichal"
            ],
        )

        self._tran = {
            "mqtt": {
                "options": {
                    "realm": "realm1",
                    "role": "anonymous",
                    "payload_mapping": {
                        "": {
                            "type": "dynamic",
                            "realm": "codec",
                            "encoder": "com.example.mqtt.encode",
                            "decoder": "com.example.mqtt.decode"
                        }
                    }
                }
            },
            "rawsocket": {
                "serializers": ["cbor", "msgpack", "ubjson", "json"]
            },
            "websocket": {
                "sock": {
                    "type": "websocket",
                    "auth" : {
                        "ticket" : {
                           "type" : "dynamic",
                           "authenticator" : "reflector.authentify"
                        }
                    },
                    "serializers": ["cbor", "msgpack", "ubjson", "json"]
                }
            }
        }

        self._dns = NeuroSpecs(self,'DOMAIN')
        self._org = NeuroSpecs(self,'NARROW')
        self._who = NeuroSpecs(self,'PERSON')

        for spec in [self._dns,self._org,self._who]:
            if os.environ['GHOST_MODULE'].strip()==spec.narrow.lower():
                self._curr = spec.nspace

            self._vern.append(spec.nspace)

    ports = property(lambda self: self._port)

    __INTERNAL_MAPPING__ = {
        'ports': '_port', # int
        'paths': '_path', # {}

        'langs': '_lang', # {}

        'daten': '_data', # []
        'xlets': '_xlet', # []
        'apps':  '_apps', # []
        'ocean': '_file', # {}

        'tools': '_tool', # []
        'works': '_work', # []
        'serve': '_serv', # []

        'trans': '_tran', # {}
        'hubot': '_bots', # {}
    }

    def __getitem__(self, key, default=None):
        if key in self.__INTERNAL_MAPPING__:
            nrw = self.__INTERNAL_MAPPING__[key]

            if hasattr(self, nrw):
                return getattr(self, nrw)

        return default

    def __setitem__(self, key, value):
        if key in self.__INTERNAL_MAPPING__:
            nrw = self.__INTERNAL_MAPPING__[key]

            setattr(self, nrw, value)

        return value

    def sequences(self):
        narrow,vernam = self._curr,self._vern

        for key in (
            'mind','clue','exec','hook','meta',
            'idea','flow','pool','tree','data',
        ):
            yield NeuroAspect(self, key, narrow, *vernam)

        self['paths']['/'] = {
            "type" : "static",
            "directory" : self['lands'],
            "options" : {
                "enable_directory_listing" : True,
                "mime_types" : {
                    ".svg" : "image/svg+xml"
                }
            }
        }

        pth = cpath('artefact.json')

        if os.path.exists(pth):
            try:
                cfg = load_json(pth)
            except Exception,ex:
                print pth,ex
                raise ex
                cfg = {}

            for nrw in self['langs']:
                self['langs'][nrw].enrichs(cfg.get(nrw, None))

        pth = cpath('reactor.py')

        if os.path.exists(pth):
            ctx,ns,nss = self,self._curr,self._vern

            exec (load_text(pth), globals(), locals())
            try:
                exec (load_text(pth), globals(), locals())
            except Exception,ex:
                print pth,ex
                raise ex

    def prepack(self):
        for verse in os.listdir(cpath('kobudo')):
            if os.path.isdir(cpath('kobudo',verse)):
                for alias in os.listdir(cpath('kobudo',verse)):
                    if os.path.isdir(cpath('kobudo',verse,alias)):
                        cfg = load_json(cpath('kobudo',verse,alias,'manifest.json'))

                        for key,lst in cfg.get('dependency', {}).iteritems():
                            self['langs'][key].enrichs(lst)

    def preincl(self):
        for key in self['langs']:
            data = self['langs'][key].exports()

            if self['langs'][key].FORMAT=='json':
                write_json(data, self['langs'][key].ASSEMBLY)
            elif self['langs'][key].FORMAT=='text':
                write_text(data, self['langs'][key].ASSEMBLY)

            for pth in [
                rpath('include',key),
                cpath('library',key),
            ]+[
                os.path.join(os.environ['HOME'],'codes','engine',x,key)
                for x in (
                    #'distrib',
                    'include',
                )
            ]:
                if os.path.exists(pth):
                    self['langs'][key].include(pth)

