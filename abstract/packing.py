from .helpers import *

##########################################################################

class PackageManager(Molecular):
    def initialize(self, *args, **kwargs):
        self._inc = []
        self._lst = {}

        self.trigger('prepare', *args, **kwargs)

    sources = property(lambda self: self._inc)
    listing = property(lambda self: self._lst)

    def depends(self, alias, version=None):
        if version in (None,False,0,'0'):
            version = '*'

        if alias in self._lst:
            if version!='*':
                self._lst[alias] = version
        else:
            self._lst[alias] = version

        return self

    def __getitem__(self, alias):
        return self._lst.get(alias,None)

    def release(self, alias):
        if alias in self._lst:
            del self._lst[alias]

        return self

    def enrichs(self, target):
        if type(target) in [dict]:
            for pkg,ver in target.iteritems():
                if pkg not in self._lst:
                    self.depends(pkg,ver)
        elif type(target) in [tuple,set,frozenset,list]:
            for pkg in target:
                if pkg not in self._lst:
                    self.depends(pkg)

    def include(self, target):
        if target not in self._inc:
            self._inc.append(target)

    def exclude(self, target):
        if target in self._inc:
            self._inc.remove(target)

