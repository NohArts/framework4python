from ronin.kata.bunkai.contrib import DeprecatedModelView

from ronin.kata.bunkai.model.backends.django import ModelAdmin


class ModelView(DeprecatedModelView, ModelAdmin):
    pass
