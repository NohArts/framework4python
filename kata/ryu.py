#!/usr/bin/env python

from ronin.kata.wsgi import *

#from ronin.provision.shortcuts import *

########################################################################################

#script = open('%s/etc/provision.py' % ROOT_PATH).read()

#exec script in globals(),locals()

########################################################################################

@click.group()
@click.option('--path', default=None)
@click.pass_context
def cli(ctx, *args, **kwargs):
    ctx.obj.update({
        'rpath': kwargs['path'],
        'cross': instantiate(**kwargs),
    })

########################################################################################

@cli.command('shell')
#@click.option('command', nargs='+')
@click.pass_context
def posix_bus(ctx, *args, **kwargs):
    pass

#***************************************************************************************

@cli.command('invite')
@click.option('-q', '--queue', multiple=True)
@click.pass_context
def interpreter(ctx, *args, **kwargs):
    from bpython.cli import main

    del sys.argv[1]

    #@shell_dir_exts('invite.py', globals(), locals())
    #def load_shell_exts(path, spec, glob, locl):
    #    source = open(path).read()
    #
    #    try:
    #        exec source in glob, locl
    #    except Exception,ex:
    #        print "Error raised while parsing : %s" % path
    #
    #        raise ex

    sys.exit(main(locals_=locals()))

#***************************************************************************************

@cli.command('execute')
@click.option('target', nargs='+')
@click.option('--type','-t', default='eval')
@click.option('--env','-e', multiple=True)
@click.option('--db','-d', multiple=True)
@click.option('--ext','-x', multiple=True)
@click.pass_context
def compilator(ctx, *targets, **kwargs):
    pass

########################################################################################

if __name__ == '__main__':
    cli(
        obj={
            'debug': True,
            #'key': 'value',
            'python': '/usr/bin/python',
            'pip':    '/usr/bin/pip',
        }
    )

