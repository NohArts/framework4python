from datafs.managers.manager_mongo import MongoDBManager
from datafs import DataAPI
from fs.osfs import OSFS
import os
import tempfile
import shutil

# overload unicode for python 3 compatability:

try:
        unicode = unicode
    except NameError:
        unicode = str

api = DataAPI(
         username='My Name',
         contact = 'my.email@example.com')

manager = MongoDBManager(
        database_name = 'MyDatabase',
        table_name = 'DataFiles')

manager.create_archive_table('DataFiles', raise_on_err=False)

api.attach_manager(manager)

temp = tempfile.mkdtemp()
local = OSFS(temp)

api.attach_authority('local', local)
api.default_authority 

