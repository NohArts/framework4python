#!/usr/bin/env python

import os,sys,re
import numpy, pandas
from datetime import datetime as dt

from flask import Flask
from flask.ext.bigtempo import DatastoreAPI, BigtempoAPI
from flask.ext.sqlalchemy import SQLAlchemy

import bigtempo.core

###############################################################################

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('DATABASE_URL','sqlite://')

#******************************************************************************

db = SQLAlchemy(app)
datastore = DatastoreAPI(app, db.engine)
bigtempo_engine = bigtempo.core.DatasourceEngine()

###############################################################################

@bigtempo_engine.datasource('SAMPLE')
class SampleDatasource(object):

    def evaluate(self, context, symbol, start=None, end=None):
        if start is None:
            start = dt(2000, 1, 1)

        if end is None:
            end = dt(2005, 1, 1)

        rng = pandas.date_range(start, end)
        data = numpy.random.randn(len(rng))
        return pandas.DataFrame(data, index=rng, columns=[symbol])

###############################################################################

@app.route('/')
def hello_world():
    return '''
           <h1>Welcome to the bigtempo web API example!</h1>
           Try accessing /api/bigtempo/{reference}/{symbol}<br />
           (eg.: /api/bigtempo/SAMPLE/blabla)
           '''

###############################################################################

bigtempo_web = BigtempoAPI(app, bigtempo_engine)

#******************************************************************************

if __name__ == '__main__':
    app.run(debug=True)

