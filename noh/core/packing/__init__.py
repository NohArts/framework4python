from ronin.abstract.packing import *

##########################################################################

@Factory.register('pkg','binary')
class BinaryManager(PackageManager):
    FORMAT   = 'text'
    ASSEMBLY = 'archives.pkg'

    def prepare(self, *args, **kwargs):
        pass

    def exports(self):
        resp = "\n".join(self.listing.keys())

        return resp

#*************************************************************************

@Factory.register('pkg','golang')
class GolangManager(PackageManager):
    FORMAT   = 'text'
    ASSEMBLY = 'golang.pkg'

    def prepare(self, *args, **kwargs):
        pass

    def exports(self):
        return ""

#*************************************************************************

@Factory.register('pkg','python')
class PythonManager(PackageManager):
    FORMAT   = 'text'
    ASSEMBLY = 'requirements.txt'

    def prepare(self, *args, **kwargs):
        pass

    def exports(self):
        resp = [
"cffi","crossbar","Werkzeug",

"Flask-Cache","Flask-SQLAlchemy","Flask-Mongoengine","Flask-Assets","Flask-Restful",

"social-auth-app-flask","social-auth-app-flask-mongoengine",

"apache-libcloud"
        ]

        for alias,version in self.listing.iteritems():
            entry = alias

            if version not in (None,False,0,'0'):
                entry += '==' + version

            resp.append(entry)

        return "\n".join(resp)

#*************************************************************************

@Factory.register('pkg','nodejs')
class NodejsManager(PackageManager):
    FORMAT   = 'json'
    ASSEMBLY = 'package.json'

    def prepare(self, *args, **kwargs):
        pass

    def exports(self):
        resp = load_json(spath('manifest.d','package.json'))

        resp["name"]    = "kobudo"
        resp["version"] = "0.0.1"

        for alias,version in self.listing.iteritems():
            resp['dependencies'][alias] = version

        for alias,version in {
    "autobahn": "^0.11.2",
    "coffe-script": "*"
        }.iteritems():
            resp['dependencies'][alias] = version

        resp.update({
            "main": "program/atom.js",
            "scripts": {
                "start": "electron ."
            },
            "bin": {
                "me-api-server": "./bin/www",
                "me-api-init": "./bin/init"
            },
        })

        return resp

#*************************************************************************

@Factory.register('pkg','maven')
class MavenManager(PackageManager):
    FORMAT   = 'xml'
    ASSEMBLY = 'pom.xml'

    def prepare(self, *args, **kwargs):
        pass

    def exports(self):
        resp = []

        return resp

#*************************************************************************

@Factory.register('pkg','composer')
class ComposerManager(PackageManager):
    FORMAT   = 'json'
    ASSEMBLY = 'composer.json'

    def prepare(self, *args, **kwargs):
        pass

    def exports(self):
        resp = load_json(spath('manifest.d','composer.json'))

        for alias,version in self.listing.iteritems():
            resp['require'][alias] = version

        resp.update({
        #    "main": "program/atom.js",
        #    "scripts": {
        #        "start": "electron ."
        #    },
        #    "bin": {
        #        "me-api-server": "./bin/www",
        #        "me-api-init": "./bin/init"
        #    },
        })

        return resp

