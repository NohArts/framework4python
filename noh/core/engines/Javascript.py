from ronin.abstract.shortcuts import *

#******************************************************************************

import execjs

##########################################################################

class EngineJS(Reactor.exe.Engine):
    def prepare(self, *args, **kwargs):
        self.trigger('booting', *args, **kwargs)

    #*********************************************************************

    def execute(self, source, **context):
        pass

##########################################################################

@Reactor.exe.supports('es6')
class ES6(EngineJS):
    def booting(self, *args, **kwargs):
        pass

    #*********************************************************************

    def execute(self, source, **context):
        pass

##########################################################################

@Reactor.exe.supports('ecma')
class ECMA(EngineJS):
    def booting(self, *args, **kwargs):
        pass

    #*********************************************************************

    def execute(self, source, **context):
        pass

