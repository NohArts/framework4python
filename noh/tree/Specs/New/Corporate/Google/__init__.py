from reactor.helpers import *

################################################################################

@Reactor.social.register_auth('google', 'GoogleOAuth2',
    register_url='',
    overview_url='',
mode='oauth2')
def detect_google(cfg):
    yield 'OAUTH2_KEY', cfg.get('key', '491752084902-cnj66fo100i84lmpvanbmpac0mlln528.apps.googleusercontent.com')
    yield 'OAUTH2_SECRET', cfg.get('pki', 'e5S55LkLMOIp20bqcXXb6bkH')

    yield 'OAUTH_SCOPE', [
        'https://www.googleapis.com/auth/drive',
        'https://www.googleapis.com/auth/userinfo.profile'
    ]

    yield 'PLUS_AUTH_EXTRA_ARGUMENTS', {
          'access_type': 'offline'
    }

    #'social.backends.salesforce.',
    #'social.backends.amazon.AmazonOAuth2',

