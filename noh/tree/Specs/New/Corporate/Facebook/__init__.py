from reactor.helpers import *

################################################################################

@Reactor.social.register_auth('facebook', 'FacebookOAuth2',
    register_url='',
    overview_url='',
mode='oauth2')
def detect_facebook(cfg):
    yield 'KEY', cfg.get('key', '1761961887348597')
    yield 'SECRET', cfg.get('pki', '82e780031dd3b97e21cdf4762bd4b041')

    yield 'SCOPE', [
        'public_profile',
        'user_friends',
        'email',
        'user_about_me',
        'user_actions.books',
        'user_actions.fitness',
        'user_actions.music',
        'user_actions.news',
        'user_actions.video',
        'user_events',
        'user_birthday',
        'user_education_history',
        'user_games_activity',
        'user_hometown',
        'user_likes',
        'user_location',
        'user_managed_groups',
        'user_photos',
        'user_posts',
        'user_relationships',
        'user_relationship_details',
        'user_religion_politics',
        'user_tagged_places',
        'user_videos',
        'user_website',
        'user_work_history',
        'read_insights',
        'read_audience_network_insights',
        'read_page_mailboxes',
        'manage_pages',
        'publish_pages',
        'publish_actions',
        'rsvp_event',
        'pages_show_list',
        'pages_manage_cta',
        'pages_manage_instant_articles',
        'ads_read',
        'ads_management',
        'pages_messaging',
        'pages_messaging_phone_number',
    ]

    yield 'PROFILE_EXTRA_PARAMS', {
        'locale': 'en_EN',
        'fields': 'id, name, email, location'
    }

#*******************************************************************************

@Reactor.social.register_auth('instagram', 'InstagramOAuth2',
    register_url='',
    overview_url='',
mode='oauth2')
def detect_instagram(cfg):
    yield 'KEY', cfg.get('key', 'c5188255944b412f9e15245e60867f14')
    yield 'SECRET', cfg.get('pki', '04497bc33dfd4febbe74710c0eb133f9')

    yield 'AUTH_EXTRA_ARGUMENTS', {
        'scope': 'likes comments relationships follower_list',
    }

