from reactor.helpers import *

################################################################################

@Reactor.social.register_auth('yahoo', 'YahooOAuth2',
    register_url='',
    overview_url='',
mode='oauth2')
def detect_yahoo(cfg):
    yield 'OAUTH2_KEY', cfg.get('key', 'dj0yJmk9MjQ4dWZhWW1uY2ZQJmQ9WVdrOWNVcG1NVVV3TkdrbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD04Yw--')
    yield 'OAUTH2_SECRET', cfg.get('pki', '8a0eb9e31588ba692d8a14e8decd8ef27b6e4622')

#*******************************************************************************

@Reactor.social.register_auth('flickr', 'FlickrOAuth',
    register_url='',
    overview_url='',
mode='oauth2')
def detect_flickr(cfg):
    yield 'KEY', cfg.get('key', '9cc43b49d7e8c3cd5b62298e3f1d3705')
    yield 'SECRET', cfg.get('pki', '168e7e0b0eaee487')

    yield 'AUTH_EXTRA_ARGUMENTS', {'perms': 'read'}

#*******************************************************************************

@Reactor.social.register_auth('tumblr', 'TumblrOAuth',
    register_url='http://www.tumblr.com/oauth/apps',
    overview_url='',
mode='oauth2')
def detect_tumblr(cfg):
    yield 'KEY', cfg.get('key', '4W8IRaN0NaUJjnEoQt4YOWSm4AwYvIkWxcfUVzA4RD8HddNzAU')
    yield 'SECRET', cfg.get('pki', '16RL1CpfEZdZYwXUzOrJxXb4hiMFuA1XMPRm3SqHO2MPXbEXlS')

