from reactor.helpers import *

################################################################################

@Reactor.social.register_auth('uber', 'UberOAuth2',
    register_url='',
    overview_url='',
mode='oauth2')
def detect_uber(cfg):
    yield 'KEY', cfg.get('key', 'CY5qKtceL8xyUQTgRnaa5-ozZJuUGIVC')
    yield 'SECRET', cfg.get('pki', 'CY5qKtceL8xyUQTgRnaa5-ozZJuUGIVC')

    yield 'SCOPE', ['profile', 'request']

#*******************************************************************************

@Reactor.social.register_auth('upwork', 'UpworkOAuth',
    register_url='',
    overview_url='',
mode='oauth2')
def detect_upwork(cfg):
    yield 'KEY', cfg.get('key', '5fafdd408e31f7584f2f64cdcd6d8bdf')
    yield 'SECRET', cfg.get('pki', 'c1d257a2737f48cb')

#*******************************************************************************

@Reactor.social.register_auth('xing', 'XingOAuth')
def detect_xing(cfg):
    yield 'KEY', cfg.get('key', 'xxxxxx')
    yield 'SECRET', cfg.get('pki', 'xxxxxx')

