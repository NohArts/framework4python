from ronin.abstract.working import *

##########################################################################

@Factory.register('rdf','fuseki')
class Fuseki(Instance):
    ASSEMBLY = 'archives.pkg'

    def prepare(self, *args, **kwargs):
        self._work = {
        }

    def exports(self):
        resp = "\n".join(self.listing.keys())

        return resp

#*************************************************************************

@Factory.register('rdf','rdf4j')
class RDF4j(Instance):
    ASSEMBLY = 'golang.pkg'

    def prepare(self, *args, **kwargs):
        self._work = {
        }

    def exports(self):
        return resp

