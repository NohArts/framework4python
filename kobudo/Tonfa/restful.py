from ronin.kata.practice import *

################################################################################

@Reactor.wsgi.register('/compoz/function.json')
@login_required
def ajax_function():
    resp = []

    return jsonify(resp)

@Reactor.wsgi.register('/compoz/pipeline.json')
@login_required
def ajax_pipeline():
    resp = []

    return jsonify(resp)

@Reactor.wsgi.register('/compoz/topology.json')
@login_required
def ajax_topology():
    resp = []

    return jsonify(resp)

##########################################################################

resource_fields = {
    'task':   fields.String,
    'uri':    fields.Url('todo_ep')
}

class TodoDao(object):
    def __init__(self, todo_id, task):
        self.todo_id = todo_id
        self.task = task

        # This field will not be sent in the response
        self.status = 'active'

@Reactor.wsgi.register('/todo/')
class Todo(Reactor.wsgi.Resource):
    @marshal_with(resource_fields)
    def get(self, **kwargs):
        return TodoDao(todo_id='my_todo', task='Remember the milk')

todos = {}

@Reactor.wsgi.register('/todo/<string:todo_id>')
class TodoSimple(Reactor.wsgi.Resource):
    def get(self, todo_id):
        return {todo_id: todos[todo_id]}

    def put(self, todo_id):
        todos[todo_id] = request.form['data']
        return {todo_id: todos[todo_id]}

