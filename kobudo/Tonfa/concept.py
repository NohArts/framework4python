from ronin.kata.practice import *

################################################################################

@Reactor.data.register(None, category="Coding")
class Runtime(Reactor.data.Document):
    alias    = StringField(max_length=200)
    enable  = BooleanField(default=True)

    def execute(self, code, **context):
        if self.alias=='python':
            exec (code,globals(),context)

        elif self.alias=='execjs':
            pass

        else:
            raise NotImplemented()

    #**************************************************************************

    def __str__(self):     return str(self.alias)
    def __unicode__(self): return unicode(self.alias)

    class Admin(Reactor.wsgi.DocumentAdmin):
        list_display = ('alias','enabled')
        # only = ('username',)

################################################################################

@Reactor.data.register(None, category="Coding")
class Library(Reactor.data.Document):
    engine   = ReferenceField(Runtime)
    alias    = StringField(max_length=200)

    #**************************************************************************

    def __str__(self):     return str(self.alias)
    def __unicode__(self): return unicode(self.alias)

    class Admin(Reactor.wsgi.DocumentAdmin):
        list_display = ('alias','engine')
        # only = ('username',)

################################################################################

@Reactor.data.register(None, category="Coding")
class Snippet(Reactor.data.Document):
    author   = ReferenceField(Identity)
    engine   = ReferenceField(Runtime)

    alias    = StringField(max_length=200)
    public   = BooleanField(default=True)

    content  = StringField()
    depends  = ListField(ReferenceField(Library),required=False)

    rpath    = property(lambda self: RPATH('workdir','eng',self.runner.alias,'snippets',self.alias))

    def persist(self):
        with open(self.rpath) as f:
            f.write(self.content)

    def execute(self, **context):
        self.runner(self.content, **context)

    #**************************************************************************

    class Admin(Reactor.wsgi.DocumentAdmin):
        list_display = ('alias','engine','author','rpath','depends')
        # only = ('username',)

