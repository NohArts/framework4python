from ronin.kata.practice import *

import nltk

from nltk.stem.lancaster import LancasterStemmer
from nltk.stem.porter import PorterStemmer
from nltk.stem.snowball import EnglishStemmer
from nltk.stem import WordNetLemmatizer
#from nltk.tag.stanford import StanfordNERTagger

################################################################################

error_code={}
error_code[701]="Empty Parameters"
error_code[702]="Error in executing this data"
error_code[703]="Data not in JSON format"
error_code[704]="Required NLTK corpus not found"
error_code[705]="Required files for Stanford NER not found"
error_code[404]="Page Not Found"

nltk.data.path.append(os.getcwd()+'/nltk-data')

LancasterSt = LancasterStemmer()
PorterSt = PorterStemmer()
SnowballSt = EnglishStemmer()
WordnetLm = WordNetLemmatizer()

################################################################################

def ret_success(result):
	res={}
	res["status"]="success"
	res["result"]=result
	return jsonify(res)

def ret_failure(code):
	res={}
	res["status"]="error"
	res["error_code"]=code
	res["error_message"]=error_code[code]
	return jsonify(res)

def parse_input(data):
	try:
		data = json.loads(data)
		return data
	except:
		return False

def penn_to_wn(tag):
	if tag in ['n','v','r','a']:
		return tag
	elif tag in ['NN', 'NNS', 'NNP', 'NNPS']:
		return 'n'
	elif tag in ['VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ']:
		return 'v'
	elif tag in ['RB', 'RBR', 'RBS']:
		return 'r'
	elif tag in ['JJ', 'JJR', 'JJS']:
		return 'a'
	else:
		return 'n'

################################################################################

def word_tokenize(data):
	if data == None:
		return ret_failure(701)
	try:
		res = nltk.word_tokenize(data)
		return ret_success(res)
	except:
		return ret_failure(701)


def sent_tokenize(data):
	if data == None:
		return ret_failure(701)
	try:
		res = nltk.sent_tokenize(data)
		return ret_success(res)
	except:
		return ret_failure(702)

def pos_tag(data):
	data = parse_input(data)
	if data == False:
		return ret_failure(703)
	else:
		try:
			res = nltk.pos_tag(data)
			return ret_success(res)
		except LookupError: 
			return ret_failure(704)
		except:
			return ret_failure(702)

def tagger(data):
	try:
		st=StanfordNERTagger('./nltk-data/StanfordNER/english.all.3class.distsim.crf.ser.gz','./nltk-data/StanfordNER/stanford-ner.jar')
	except:
		return ret_failure(705)
	#try:
	tag = st.tag(data.split())
	#except:
	#	return ret_failure(702)
	return ret_success(tag)

################################################################################

def stemmer(method,data):
	"""
	Takes an array of words in JSON format.
	"""
	data = parse_input(data)
	if data == False:
		return ret_failure(703)
	else:
		res=[]
		if method == "lancaster":
			for word in data:
				try:
					res.append([word,LancasterSt.stem(word)])
				except:
					return ret_failure(702)
		elif method == "porter":
			for word in data:
				try:
					res.append([word,PorterSt.stem(word)])
				except:
					return ret_failure(702)
		elif method == 'snowball':
			for word in data:
				try:
					res.append([word,SnowballSt.stem(word)])
				except:
					return ret_failure(702)
		else:
			abort(404)
		return ret_success(res)

def lemmatize(method,data):
	"""
	Takes an array of words or array of tupples containing words and pos tags.
	Both Penn and Wordnet tags are supported
	"""
	data = parse_input(data)
	if data == False:
		return ret_failure(703)
	else:
		res=[]
		if method == "wordnet":
			for word in data:
				try:
					if type(word) is list:
						res.append([word[0],WordnetLm.lemmatize(word[0],penn_to_wn(word[1]))])
					else:	
						res.append([word,WordnetLm.lemmatize(word)])
				except LookupError: 
					return ret_failure(704)
				except:
					return ret_failure(702)
		else:
			abort(404)
		return ret_success(res)

