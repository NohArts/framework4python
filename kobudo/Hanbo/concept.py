from ronin.kata.practice import *

################################################################################

CLOUD_SERVICEs = [
    ('heroku',  'Heroku.com'),

    ('mega.nz', 'Mega.nz'),

    ('trakt',   'Trakt.tv'),
]

@Reactor.data.register(None, category="Daten")
class WebService(Reactor.data.Document):
    owner = ReferenceField(Identity)
    cloud = StringField(max_length=50,choices=CLOUD_SERVICEs)

    email = EmailField()
    login = StringField(max_length=50,required=False)
    token = StringField(max_length=512)

    state = BooleanField(required=False)

    def is_active(self):
        return self.active

    #**************************************************************************

    class Admin(Reactor.wsgi.DocumentAdmin):
        list_display = ('alias','engine')
        # only = ('username',)

#*************************************************************************

PROTOCOLs = [
    dict(name='http',     text='HTTP-based backend'),

    dict(name='memcache', text='Memcache instance'),
    dict(name='redis',    text='Redis instance'),

    dict(name='postgres', text='PostgreSQL database'),
    dict(name='mysql',    text='MySQL database'),
    dict(name='mongodb',  text='MongoDB database'),

    dict(name='neo4j',    text='Neo4j graph store'),
    dict(name='rdf',      text='RDF Graph store'),

    dict(name='amqp',     text='AMQP broker'),
    dict(name='mqtt',     text='MQQT broker'),

    dict(name='wamp',     text='WAMP broker'),
]

@Reactor.wsgi.register(None,category="coding")
class Backend(Reactor.data.Document):
    owner  = ReferenceField(Identity)
    alias  = StringField(max_length=50)
    proto  = property(lambda self: urlparse(self.target))

    target = URLField()
    active = BooleanField(default=True)
    origin = StringField(max_length=200, default='admin')
    source = StringField(max_length=200, default='cloud')

    @property
    def protocol(self):
        nrw = self.parsed.scheme

        key = {
            'https':'http',
        }.get(nrw,nrw)

        obj = None

        for proto in PROTOCOLs:
            if proto['name']==key:
                return proto

        return dict(name='misc', text='Unknown provider')

    parsed = property(lambda self: urlparse(self.target))

    def is_active(self):
        return self.active

    #**************************************************************************

    class Admin(Reactor.wsgi.DocumentAdmin):
        list_display = ('alias','proto','owner','active','target','origin','source')
        # only = ('username',)

#*************************************************************************

#@Reactor.wsgi.register(None, category="")
class Drive(Reactor.data.Document):
    owner    = ReferenceField(Identity)
    account  = EmailField()
    enabled  = BooleanField(default=True)

    provider = StringField(max_length=200)
    username = StringField(max_length=200)
    password = StringField(max_length=200, default='')

