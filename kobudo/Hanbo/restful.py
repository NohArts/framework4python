from ronin.kata.practice import *

################################################################################

@Reactor.wsgi.register('/linked/namespaces.json')
@login_required
def ajax_namespace():
    resp = []

    return jsonify(resp)

@Reactor.wsgi.register('/linked/endpoints.json')
@login_required
def ajax_endpoints():
    resp = []

    return jsonify(resp)

@Reactor.wsgi.register('/linked/ontologies.json')
@login_required
def ajax_ontologic():
    resp = []

    return jsonify(resp)

