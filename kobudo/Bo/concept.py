from ronin.kata.practice import *

################################################################################

@Reactor.data.register(None, category="Cables")
class Bookmark(Reactor.data.Document):
    owner    = ReferenceField(Identity)
    username = StringField(max_length=200)
    password = StringField(max_length=200, default='')
    name = StringField(max_length=100)
    email = EmailField()
    active = BooleanField(default=True)

    def is_active(self):
        return self.active

#*************************************************************************

@Reactor.data.register(None, category="Cables")
class NewsFeed(Reactor.data.Document):
    owner    = ReferenceField(Identity)
    account  = EmailField()
    enabled  = BooleanField(default=True)

    provider = StringField(max_length=200)
    username = StringField(max_length=200)
    password = StringField(max_length=200, default='')

