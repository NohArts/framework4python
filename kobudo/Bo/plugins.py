from ronin.kata.practice import *

################################################################################

#@Reactor.wamp.register_middleware('vital.Breath')
class Breath(WampAppSession):
   @WampCallbacks
   def onJoin(self, details):
        counter = 0
        pneumo  = True

        while True:
            self.publish('tohoku.body.heart.cardio', [str(counter)])

            if counter % 10:
                if pneumo:
                    self.publish('tohoku.body.pneumo.respire')
                else:
                    self.publish('tohoku.body.pneumo.expire')

                pneumo = not pneumo

            counter += 1

            yield sleep(0.8)

