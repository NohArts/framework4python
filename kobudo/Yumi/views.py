from ronin.kata.practice import *

from .helpers import *

##########################################################################

@Reactor.wsgi.register('/need-email')
def require_email():
    strategy = load_strategy()
    partial_token = request.args.get('partial_token')
    partial = strategy.partial_load(partial_token)
    return render_template('social/homepage.html',
                           email_required=True,
                           partial_backend_name=partial.backend,
                           partial_token=partial_token)

#*************************************************************************

@Reactor.wsgi.register('/logout/?')
@login_required
def logout():
    """Logout view"""
    logout_user()
    return redirect('/')

#*************************************************************************

@Reactor.wsgi.register('/connect')
class JoinUs(Reactor.wsgi.Resource):
    def get(self):
        return render_page('special', 'connect', None, title="Join us using your social accounts")

#*************************************************************************

@Reactor.wsgi.register('/accounts')
#@login_required
class AccountSocial(Reactor.wsgi.Resource):
    def get(self):
        return render_page(None, 'accounts', None, title="Online accounts")

from social_flask import routes

##########################################################################

@Reactor.wsgi.register('/welcome')
class WelcomeProfile(Reactor.wsgi.Resource):
    def get(self):
        return render_page(None, 'welcome', None, title="Welcome Dashboard")

#*************************************************************************

@Reactor.wsgi.register('/profile')
class AccountProfile(Reactor.wsgi.Resource):
    def get(self):
        return render_page(None, 'profile', None, title="Account profile")

#*************************************************************************

@Reactor.wsgi.register('/settings')
class AccountSettings(Reactor.wsgi.Resource):
    def get(self):
        cnt = {
            'backends': [],
        }

        for item in [
            dict(name='hubot', help=""),
            dict(name='parse', help=""),
        ]:
            g.user.backends

            cnt['backends'].append(item)

        return render_page(None, 'settings', cnt, title="Account settings")

    def post(self):
        return render_page(None, 'settings', None, title="Account settings")

##########################################################################

@Reactor.wsgi.register('/-<provider>/')
class Cloud_Provider_Home(Reactor.wsgi.Resource):
    def get(self,provider):
        return render_page('oceans', 'overview', None, title="Cloud Provider",
            provider = provider,
        )

#*************************************************************************

@Reactor.wsgi.register('/-<provider>/setup')
class Cloud_Provider_Setup(Reactor.wsgi.Resource):
    def get(self,provider):
        return render_page('oceans', 'settings', None, title="Cloud Settings",
            provider = provider,
        )

##########################################################################

@Reactor.wsgi.register('/-<provider>/@narrow')
class Cloud_Person_List(Reactor.wsgi.Resource):
    def get(self,provider,entity):
        return render_page('oceans', 'person', None, title="Cloud Resource")

#*************************************************************************

@Reactor.wsgi.register('/-<provider>/~<narrow>')
class Cloud_Agent_View(Reactor.wsgi.Resource):
    def get(self,provider,narrow):
        return render_page('oceans', 'agent', None, title="Cloud Resource")

##########################################################################

@Reactor.wsgi.register('/-<provider>/-<entity>/')
class Cloud_Particle_View(Reactor.wsgi.Resource):
    def get(self,provider,entity):
        return render_page('oceans', 'particle', None, title="Cloud Particle")

#*************************************************************************

@Reactor.wsgi.register('/-<provider>/-<entity>/<narrow>')
class Cloud_Resource_View(Reactor.wsgi.Resource):
    def get(self,provider,entity,narrow):
        return render_page('oceans', 'resource', None, title="Cloud Resource")

