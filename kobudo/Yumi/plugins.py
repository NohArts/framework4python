from ronin.kata.practice import *

################################################################################

#@Reactor.wamp.register_middleware('gateway.Native')
class Authority(WampAppSession):
    #@Reactor.wamp.register_topic(u'console')
    def error_reporting(self, module, level, message):
        if level in ('debug', 'info', 'warning', 'error'):
            #print("[{}] {} [{}] {}".format(datetime.now(), module, level, message))

            print("[{}] {}".format(module, message))

        if module in ('reactor', 'hub', 'info'):
            pass # to file

    #***************************************************************************

    #@Reactor.wamp.register_topic(u'process')
    def shell_commands(self, args, command, stdout):
        print("[{}] <{}> [STDOUT] {}".format(datetime.now(), 'shell', command, stdout))

    ############################################################################

    #@Reactor.wamp.register_method(u'logging')
    def log_to(self, *args, **kwargs):
        return self.log(*args, **kwargs)

    #***************************************************************************

    #@Reactor.wamp.register_method(u'execute')
    def run_shell(self, *args, **kwargs):
        return self.shell(*args, **kwargs)

