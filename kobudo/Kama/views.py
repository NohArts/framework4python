from ronin.kata.practice import *

################################################################################

@Reactor.wsgi.register('/coding/')
class Coding_Home(Reactor.wsgi.Resource):
    def get(self):
        return render_page('coding', 'homepage', None, title="")

##########################################################################

@Reactor.wsgi.register('/oceans/')
class Oceans_Home(Reactor.wsgi.Resource):
    def get(self):
        return render_page('oceans', 'homepage', None, title="")

##########################################################################

@Reactor.wsgi.register('/opsys/')
class OpSys_Home(Reactor.wsgi.Resource):
    def get(self):
        return render_page('opsys', 'homepage', None, title="")

