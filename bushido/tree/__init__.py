from ronin.abstract.shortcuts import *

#******************************************************************************

#from datafs import DataAPI

##########################################################################

@Reactor.extend('apis')
class Manager(Reactor.Extension):
    def prepare(self):
        self._que = {}

        #self._api = DataAPI(
        #     username='My Name',
        #     contact = 'my.email@example.com')

    #********************************************************************

    jobs = property(lambda self: self._vfs)
    apis = property(lambda self: self._api)

    #####################################################################

    class Provider(Atomic):
        def initialize(self, alias):
            self._key = alias

        alias = property(lambda self: self._key)

        @property
        def title(self):
            name = self.alias
            name = name.replace('OAuth2', '')
            name = name.replace('OAuth1', '')
            name = name.replace('OAuth', '')
            name = name.replace('OpenId', '')
            name = name.replace('Sandbox', '')
            name = name.replace('Auth', '')
            name = name.replace('V2', '')
            name = NAME_RE.sub(r'\1 Auth', name)
            return name

        @property
        def icon(self):
            name = self.alias

            for key in ('-oauth2','-oauth','-app'):
                if name.endswith(key):
                    name = name.replace(key,'')

            name = {
                'google-openidconnect': 'google',
                'stackoverflow': 'stack-overflow',

                'live': 'windows',
                'pocket': 'get-pocket',
                'vimeo': 'vimeo-square',
            }.get(name, name)

            return {
                'email': 'envelope',
                'username': 'user',

                'docker': 'rocket',
                'coinbase': 'bitcoin',
                'upwork': 'briefcase',
                'podio': 'podcast',
                'uber': 'taxi',
                'qiita': 'quora',
                'wunderlist': 'book',
                'shopify': 'shopping-cart',
            }.get(name, name)

    #********************************************************************

    class Session(Atomic):
        def initialize(self, identity, provider):
            self._user = identity
            self._prvd = provider

        identity = property(lambda self: self._user)
        provider = property(lambda self: self._prvd)

        alias    = property(lambda self: self.provider.alias)

        @property
        def association(self):
            try:
                return self.identity.social_auth.get(provider=self.alias)
            except:
                return None

        @property
        def credentials(self):
            assoc = self.association

            resp = {}

            if assoc is not None:
                resp = assoc.extra_data
            
            if 'access_token' in resp:
                resp = resp['access_token']
            
            return resp

    #####################################################################

    class Entity(Atomic):
        MAPPING = {
        }

        def initialize(self, session, alias):
            self._prx = session
            self._key = alias

            self._css = {}
            self._lnk = {}

            for cfg in [
                dict(query=[
                    'user','profile','person',
                ], style=dict(box='primary', uni='md', col='3'), links={
                    'home': "/-%(backend)s/@%(entity)s/",
                }),
                dict(query=[
                    'team','page','brand',
                ], style=dict(box='info', uni='md', col='4'), links={
                    'home': "/-%(backend)s/~%(entity)s/",
                }),
            ]:
                if alias in cfg['query']:
                    self._css.update(cfg['style'])

                    for key in cfg['links']:
                        self._lnk[key] = cfg['links'][key] % dict(
                            backend = session.alias,
                            entity  = alias,
                        )

        session  = property(lambda self: self._prx)
        alias    = property(lambda self: self._key)
        links    = property(lambda self: self._lnk)
        styles   = property(lambda self: self._css)

        identity = property(lambda self: self.session.identity)
        provider = property(lambda self: self.session.provider)
        backend  = property(lambda self: self.session.alias)

        @property
        def schema(self):
            pth = RPATH('ronin','kyodo',self.backend,'')

    #####################################################################

    class Resource(Atomic):
        def initialize(self, alias):
            self._key = alias

        alias = property(lambda self: self._key)

##########################################################################

@Reactor.extend('tree')
class Manager(Reactor.Extension):
    def prepare(self):
        self._que = {}

        #self._api = DataAPI(
        #     username='My Name',
        #     contact = 'my.email@example.com')

    #********************************************************************

    jobs = property(lambda self: self._vfs)
    apis = property(lambda self: self._api)

    #####################################################################

    class Provider(Atomic):
        pass

    #********************************************************************

    class Endpoint(Atomic):
        pass

    #####################################################################

    class Plugin(Atomic):
        pass

    #********************************************************************

    class Factory(Atomic):
        pass

