from ronin.kata.helpers import *

from .helpers import *

from . import filters

#******************************************************************************

from flask_apscheduler import APScheduler

from ronin.kata.bunkai import Admin as FlaskAdmin

from ronin.kata.bunkai import BaseView, expose
from ronin.kata.bunkai.model.backends.sqlalchemy  import ModelAdmin as BaseModelAdmin
from ronin.kata.bunkai.model.backends.mongoengine import ModelAdmin as BaseDocumentAdmin

############################################################################

#@Reactor.wsgi.extend(None,'flask_gzip','Gzip')
@Reactor.wsgi.extend(None,'flask_rq','RQ')
@Reactor.wsgi.extend(None,'flask_analytics','Analytics')
@Reactor.wsgi.extend(None,'flask_bcrypt','Bcrypt')
#@Reactor.wsgi.extend(None,'flask_profiler','Profiler')
def mw_dummy(app,ext):
    pass

#******************************************************************************

@Reactor.wsgi.extend(None,'flask_redis','FlaskRedis')
def mw_cacher(app,ext):
    pass

#******************************************************************************

@Reactor.wsgi.extend(None,'flask_mail','Mail')
def mw_mailer(app,ext):
    pass

############################################################################

#@Reactor.wsgi.extend(None,'flask_sslify','SSLify',subdomains=True,permanent=True,skips=['mypath', 'anotherpath'])
def mw_sslify(app,ext):
    pass

#******************************************************************************

@Reactor.wsgi.extend(None,'flask_debugtoolbar','DebugToolbarExtension')
def mw_debugger(app,ext):
    app.config['flask_profiler'] = {
        "enabled": True, # app.config['DEBUG'],
        "endpointRoot": "debug",
        "storage": {
            "engine":     "mongodb",
            "MONGO_URL":  Reactor.data.backend('brain').geturl(),
            "DATABASE":   app.config['MONGODB_SETTINGS']['db'],
            "COLLECTION": 'debug_profiler',
        },
        "basicAuth":{
            "enabled": True,
            "username": "admin",
            "password": "admin",
        },
        "ignore": [
            "^/static/.*"
        ]
    }

############################################################################

#bigtempo_engine = bigtempo.core.DatasourceEngine()
#datastore = DatastoreAPI(app, db.engine)

#bigtempo_web = BigtempoAPI(app, bigtempo_engine)

############################################################################

@Reactor.wsgi.app.before_request
def global_user():
    # evaluate proxy value
    g.user = current_user._get_current_object()

############################################################################

@Reactor.wsgi.idp.user_loader
def load_user(userid):
    from ghost.internals import Identity

    try:
        return Identity.objects.get(id=userid)
    except (TypeError, ValueError):
        pass

#******************************************************************************

@Reactor.wsgi.app.context_processor
def load_common_context():
    return common_context(
        Reactor.wsgi.app.config['SOCIAL_AUTH_AUTHENTICATION_BACKENDS'],
        load_strategy(),
        getattr(g, 'user', None),
        Reactor.wsgi.app.config.get('SOCIAL_AUTH_GOOGLE_PLUS_KEY')
    )

Reactor.wsgi.app.context_processor(backends)

############################################################################

@Reactor.wsgi.app.context_processor
def enrich_ui():
    resp = {
        'user': None,
    }

    try:
        resp['user'] = g.user
    except AttributeError:
        pass

    return resp

#******************************************************************************

Reactor.wsgi.app.jinja_env.globals['url'] = common_url_for

#Reactor.wsgi.app.jinja_env.undefined = jinja2.Undefined

