from ronin.abstract.shortcuts import *

#******************************************************************************

#from datafs import DataAPI

##########################################################################

@Reactor.extend('exe')
class Manager(Reactor.Extension):
    def prepare(self):
        self._eng = {}

        #self._api = DataAPI(
        #     username='My Name',
        #     contact = 'my.email@example.com')

    #********************************************************************

    engines = property(lambda self: self._eng)

    def __getitem__(self, key, default=None):
        return self._eng.get(key, default)

    #********************************************************************

    def supports(self, *args, **kwargs):
        def do_apply(handler, *alias):
            for key in alias:
                if key not in self._eng:
                    self._eng[key] = handler(self, key)

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    #####################################################################

    class Script(Atomic):
        pass

    #********************************************************************

    class Engine(Atomic):
        def initialize(self, *args, **kwargs):
            self._cnt = {}
            self._rpc = {}

            self.trigger('prepare', *args, **kwargs)

        context = property(lambda self: self._cnt)
        methods = property(lambda self: self._rpc)

        #****************************************************************

        def __getitem__(self, key, default=None):
            return self._cnt.get(key, default)

        def __setitem__(self, key, value):
            self._cnt[key] = value

            return value

        #****************************************************************

        def register(self, *args, **kwargs):
            def do_apply(handler, alias):
                if alias not in self._rpc:
                    self._rpc[alias] = handler

                return handler

            return lambda hnd: do_apply(hnd, *args, **kwargs)

        def __invoke__(self, method, *args, **kwargs):
            hnd = self._rpc.get(method, None)

            if callable(hnd):
                return hnd(*args, **kwargs)

        #****************************************************************

        def __call__(self, *sources, **context):
            for target in sources:
                yield self.execute(*target, **context)

    #####################################################################

    class Ripper(Atomic):
        pass

    #********************************************************************

    class Runtime(Atomic):
        pass

