from ronin.abstract.shortcuts import *

#******************************************************************************

#from datafs import DataAPI

##########################################################################

@Reactor.extend('meta')
class Manager(Reactor.Extension):
    def prepare(self):
        self._que = {}

        #self._api = DataAPI(
        #     username='My Name',
        #     contact = 'my.email@example.com')

    #********************************************************************

    jobs = property(lambda self: self._vfs)
    apis = property(lambda self: self._api)

    #####################################################################

    class Subject(Atomic):
        pass

    #********************************************************************

    class xxxxxxx(Atomic):
        pass

    #####################################################################

    class Fact(Atomic):
        pass

    #********************************************************************

    class Vocabulary(Atomic):
        pass

