from ronin.abstract.shortcuts import *

#******************************************************************************

#from datafs import DataAPI

##########################################################################

@Reactor.extend('idea')
class Manager(Reactor.Extension):
    def prepare(self):
        self._que = {}

        #self._api = DataAPI(
        #     username='My Name',
        #     contact = 'my.email@example.com')

    #********************************************************************

    jobs = property(lambda self: self._vfs)
    apis = property(lambda self: self._api)

    #####################################################################

    class Structure(Atomic):
        pass

    #********************************************************************

    class Streamer(Atomic):
        pass

    #####################################################################

    class Feed(Atomic):
        pass

    #********************************************************************

    class xxxxxx(Atomic):
        pass

