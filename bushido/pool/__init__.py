from ronin.abstract.shortcuts import *

#******************************************************************************

#from datafs import DataAPI

##########################################################################

@Reactor.extend('pool')
class Manager(Reactor.Extension):
    def prepare(self):
        self._que = {}

        #self._api = DataAPI(
        #     username='My Name',
        #     contact = 'my.email@example.com')

    #********************************************************************

    jobs = property(lambda self: self._vfs)
    apis = property(lambda self: self._api)

    #####################################################################

    class Job(Atomic):
        pass

    #********************************************************************

    class Dispatch(Atomic):
        pass

    #####################################################################

    class Manager(Atomic):
        pass

    #********************************************************************

    class Trigger(Atomic):
        pass

