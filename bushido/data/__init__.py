from ronin.abstract.shortcuts import *

#******************************************************************************


################################################################################

@Reactor.extend('data')
class Daten_Helper(Reactor.Extension):
    def prepare(self):
        self._url = {}
        self._bkn = {}

        self._mdl = {}
        self._sch = {}
        self._nod = {}
        self._rel = {}

        self._xsd = {}
        self._orm = {}
        self._odm = {}
        self._ogm = {}

        for key in os.environ:
            if key.endswith('_URL'):
                nrw = key.replace('_URL','').lower()

                raw = os.environ[key]

                if raw.startswith("'"):
                    if raw.endswith("'"):
                        raw= raw[1:-1]

                self._url[nrw] = raw

        for nrw,lst in [
            ('cache', ['rediscloud','redis']),
            ('focus', ['redistogo','redis']),
            ('store', ['elephant','database']),
            ('brain', ['mongolab','mongodb']),
            ('graph', ['graphenedb','neo4j']),
            ('facts', ['triples','fuseki']),

            ('queue',  ['cloudamqp','rabbitmq']),
            ('topic',  ['cloudamqp','mqtt']),
        ]:
            if nrw not in self._bkn:
                self._bkn[nrw] = []

            for key in [nrw]+lst:
                if key in self._url:
                    try:
                        link = urlparse(self._url[key])
                    except:
                        link = None

                    if link is not None:
                        self._bkn[nrw].append(link)

    env_urls = property(lambda self: self._url)
    backends = property(lambda self: self._bkn)

    #******************************************************************************

    def backend(self, alias, default=None):
        if alias in self.backends:
            lst = self.backends[alias]

            if len(lst):
                return self.backends[alias][0]

        if type(default) in (str,unicode):
            return urlparse(default)
        else:
            return None

    #####################################################################

    def pre_flask(self, app):
        store = self.backend('store','postgres://%(GHOST_PERSON)s:@localhost:5432/%(GHOST_MODULE)s' % os.environ)
        brain = self.backend('brain','mongodb://localhost:27017/%(GHOST_MODULE)s' % os.environ)

        cache = self.backend('cache','redis://localhost:6379/1')
        focus = self.backend('focus','redis://localhost:6379/2')

        queue = self.backend('queue','amqp://localhost:5672/%(GHOST_MODULE)s' % os.environ)
        topic = self.backend('topic','mqtt://localhost:1883/')

        #************************************************************************

        app.config['SQLALCHEMY_DATABASE_URI'] = store.geturl()
        #app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

        app.config['MONGODB_SETTINGS'] = {
            'host':     brain.hostname,
            'port':     brain.port,
            'db':       brain.path[1:],
            'username': brain.username,
            'password': brain.password,
        }

        #************************************************************************

        app.config['RQ_DEFAULT_HOST'] = focus.hostname
        app.config['RQ_DEFAULT_PORT'] = focus.port
        app.config['RQ_DEFAULT_PASSWORD'] = focus.password
        try:
            app.config['RQ_DEFAULT_DB'] = int(focus.path[1:])
        except:
            app.config['RQ_DEFAULT_DB'] = 0

        #************************************************************************

        app.config['AMQP_HOSTNAME'] = queue.hostname
        app.config['AMQP_PORT']     = queue.port
        app.config['AMQP_USERNAME'] = queue.username
        app.config['AMQP_PASSWORD'] = queue.password

        app.config['MQTT_BROKER_URL']  = topic.hostname
        app.config['MQTT_BROKER_PORT'] = topic.port
        app.config['MQTT_USERNAME']    = topic.username
        app.config['MQTT_PASSWORD']    = topic.password

    #******************************************************************************

    def post_flask(self, app):
        from flask_sqlalchemy import SQLAlchemy

        self.sql = SQLAlchemy(app)

        setattr(Reactor.data, 'Model', self.sql.Model)

        #******************************************************************************

        from flask_mongoengine import MongoEngine

        self.nosql = MongoEngine(app)

        setattr(Reactor.data, 'Document', self.nosql.Document)

        #******************************************************************************

        self.neo = None

    #####################################################################

    models  = property(lambda self: self._mdl)
    schemas = property(lambda self: self._sch)
    nodes   = property(lambda self: self._nod)
    edges   = property(lambda self: self._rel)

    def register(self, *args, **kwargs):
        def do_apply(handler,alias,**options):
            if alias is None:
                alias = handler.__name__.capitalize()

            if issubclass(handler,self.Model):
                if alias not in self._mdl:
                    self._mdl[alias] = handler

                #if hasattr(handler,'Admin'):
                #    Reactor.wsgi.adm.register(handler, handler.Admin) #, session=nosql.session)

            elif issubclass(handler,self.Document):
                if alias not in self._sch:
                    self._sch[alias] = handler

                if hasattr(handler,'Admin'):
                    Reactor.wsgi.adm.register(handler, handler.Admin) #, session=nosql.session)

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    #####################################################################

    def populate(self):
        for obj,lst in [
            (bool,    ['bool','boolean']),
            (int,     ['int', 'integer']),
            (long,    ['long','longint']),

            (str,     ['str','string']),
            (unicode, ['txt','text']),
            (email,   ['mail','email']),
            (url,     ['url','link']),
        ]:
            for key in lst:
                self._xsd[key] = obj

        from mongoengine import StringField, EmailField, BooleanField, URLField, ListField, DictField, ReferenceField

        self._odm[bool]  = BooleanField
        self._odm[int]   = StringField
        self._odm[str]   = StringField
        self._odm[url]   = URLField
        self._odm[email] = EmailField

        self._odm[list]  = ListField
        self._odm[dict]  = DictField

        self._odm[self.Document] = ReferenceField

    def column(self, alias, *args, **kwargs):
        target = alias

        while target in self._xsd:
            target = self._xsd[target]

        if target in self._orm:
            hnd = self._orm[target]

            return hnd(*args,**kwargs)

    def field(self, alias):
        target = alias

        while target in self._xsd:
            target = self._xsd[target]

        if target in self._odm:
            hnd = self._odm[target]

            return hnd(*args,**kwargs)

    def attrib(self, alias):
        target = alias

        while target in self._xsd:
            target = self._xsd[target]

        if target in self._ogm:
            hnd = self._ogm[target]

            return hnd(*args,**kwargs)

