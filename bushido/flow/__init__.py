from ronin.abstract.shortcuts import *

#******************************************************************************

#from datafs import DataAPI

##########################################################################

@Reactor.extend('flow')
class Manager(Reactor.Extension):
    def prepare(self):
        self._que = {}

        #self._api = DataAPI(
        #     username='My Name',
        #     contact = 'my.email@example.com')

    #********************************************************************

    jobs = property(lambda self: self._vfs)
    apis = property(lambda self: self._api)

    #####################################################################

    class Operator(Atomic):
        pass

    #********************************************************************

    class Process(Atomic):
        pass

    #####################################################################

    class Backend(Atomic):
        pass

    #********************************************************************

    class Workflow(Atomic):
        pass

