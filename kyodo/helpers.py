from ronin.kata.practice import *

from social_flask.utils import load_strategy

################################################################################

class SocialAPI(Atomic):
    @classmethod
    def from_alls(cls):
        for item in stt.SOCIAL_AUTH_MAPPING.values():
            yield SocialMeta(None, **item)

    @classmethod
    def from_user(cls):
        for assoc in g.user.social_auth.all():
            yield SocialMeta(g.user, assoc.provider)

    @classmethod
    def from_name(cls,narrow):
        for item in cls.from_user():
            if item.alias==narrow:
                return item

        return None

    def initialize(self, user, alias, **config):
        self._who = user
        self._key = alias

        self._cfg = config
        self._psa = None

        if len(self.config)==0:
            if self.alias in stt.SOCIAL_AUTH_MAPPING:
                self._cfg = stt.SOCIAL_AUTH_MAPPING[self.alias]

        if len(self.config)==0:
            self._cfg = json.loads(open().read())

        self._sch = {}

        if self.assoc is not None:
            self._sch = None

    def preload(self):
        pass

    owner   = property(lambda self: self._who)

    @property
    def assoc(self):
        if self._psa is None:
            if self.owner is not None:
                try:
                    self._psa = self.owner.social_auth.get(provider=self.alias)
                except Exception,ex:
                    print ex

                    self._psa = None

        return self._psa

    alias   = property(lambda self: self._key)

    def cfg_key(self, *x): return '_'.join(['SOCIAL','AUTH',item['alias'].upper()]+[y.upper() for y in x])
    def env_key(self, *x): return '_'.join(['OAUTH',item['alias'].upper()]+[y.upper() for y in x])

    config  = property(lambda self: self._cfg)

    @property
    def environ(self):
        cfg = {}

        for key in ('KEY','PKI','SCOPE'):
            if psa_env(key) in stt.environ:
                cfg[key.lower()] = stt.environ[psa_env(key)]

        return cfg

    @property
    def context(self):
        resp = {}

        if self.assoc is not None:
            resp =  self.assoc.extra_data

        return resp

    handler  = property(lambda self: self.config.get('handle',''))
    callback = property(lambda self: '/complete/%s/' % self.alias)

    base_url = property(lambda self: self.config.get('base_url',''))
    services = property(lambda self: self.config.get('services',''))

    @property
    def entities(self):
        lst = []

        if self._sch is None:
            pth = RPATH('ronin','kyodo',self.alias,'entities.yaml')

            if os.path.exists(pth):
                for key,cfg in yaml.load(open(pth)).iteritems():
                    obj = SocialEntity(self,key,**cfg)

                    self._sch = None

        return self._sch

    ################################################################################

    class Entity(Atomic):
        def initialize(self, parent, alias, **kwargs):
            self._prn = parent
            self._key = alias

            for target,field,handle in [
                ('_col','property',dict),
                ('_arg','queryset',SocialAPI.Parameter),
                ('_ops','operates',SocialAPI.HttpQuery),
            ]:
                resp = {}

                if field in kwargs:
                    for cfg in kwargs.get(field,[]):
                        cfg['alias'] = cfg['name'] ; del cfg['name']

                        resp[key] = handle(self,key,**cfg)

                    del kwargs[field]

                setattr(self,target,resp)

            self._cfg = kwargs

            self.trigger('prepare', *args, **kwargs)

        cloud  = property(lambda self: self._prn)
        alias  = property(lambda self: self._key)

        props  = property(lambda self: self._col)
        param  = property(lambda self: self._arg)
        query  = property(lambda self: self._ops)

        config = property(lambda self: self._cfg)

        @property
        def environ(self):
            cfg = {}

            for key in ('KEY','PKI','SCOPE'):
                if psa_env(key) in stt.environ:
                    cfg[key.lower()] = stt.environ[psa_env(key)]

            return cfg

    ################################################################################

    class Parameter(Atomic):
        def initialize(self, parent, alias, **kwargs):
            self._prn = parent
            self._key = alias

            self._xsd = kwargs.get('type','string')
            self._pth = kwargs.get('path','')

        cloud = property(lambda self: self.daten.cloud)
        daten = property(lambda self: self._prn)
        alias = property(lambda self: self._key)

        dtype = property(lambda self: self._xsd)
        rpath = property(lambda self: self._pth or self.alias)

    ################################################################################

    class HttpQuery(Atomic):
        def initialize(self, parent, alias, verb, link, **kwargs):
            self._prn = parent
            self._key = alias

            self._vrb = verb
            self._lnk = link

            self._arg = kwargs.get('args',[])
            self._ret = kwargs.get('type','json')

        cloud = property(lambda self: self.daten.cloud)
        daten = property(lambda self: self._prn)
        alias = property(lambda self: self._key)

        verbe = property(lambda self: self._vrb)
        rpath = property(lambda self: self._lnk)
        param = property(lambda self: self._arg)
        rtype = property(lambda self: self._ret)

